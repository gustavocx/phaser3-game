import { CST } from "./CST.js";

// Variales

var widthSize = 1366;
var HeightSize = 768;

var gameOver = false;

var player;
var stars;
var bombs;
var platforms;
var cursors;

var score = 0;
var vidas = 3;
var scoreText;
var vidasText;

var flecha = 0;
var flechaIzquierda; // 1
var flechaDerecha; // 2
var flechaArriba; // 3
var flechaAbajo; // 4

var darVida = 100;

var backgrounSky;

var music;

var btnScreen;

var btnPause;

var masVelocidad = 25;

var masVelocidadX = 0.3;

export class PlayScene extends Phaser.Scene {

    constructor() {
        super({
            key: CST.SCENES.PLAY
        })
    }

    // Crear objetos
    create() {

        backgrounSky = this.add.tileSprite(0, 0, 0, 0, CST.IMAGE.SKY).setOrigin(0, 0);

        // ogg
        this.music = this.sound.add(CST.AUDIO.MUSIC);
        this.jump = this.sound.add(CST.AUDIO.JUMP);
        this.coin = this.sound.add(CST.AUDIO.COIN);
        this.bombOgg = this.sound.add(CST.AUDIO.BOMB);
        this.gameOver = this.sound.add(CST.AUDIO.GAMEOVER);
        this.darVidaOgg = this.sound.add(CST.AUDIO.DARVIDA);

        // hear img
        this.add.image(5, 50, CST.SVG.HEAR).setOrigin(0, 0);

        //Creamos los objetos fisicos del juego
        platforms = this.physics.add.staticGroup();
        platforms.create(683, HeightSize - 60, CST.IMAGE.SUELO);
        platforms.create(100, 600 / 1.4, CST.IMAGE.PLATFORM);
        platforms.create(widthSize - 100, 600 / 1.4, CST.IMAGE.PLATFORM);
        platforms.create(widthSize / 2, 600 / 3.3, CST.IMAGE.PLATFORM);

        // Teclado de la Computadora
        cursors = this.input.keyboard.createCursorKeys();

        // Configuramos la animacion del jugador
        this.anims.create({
            key: "left",
            frames: this.anims.generateFrameNumbers(CST.PLAYER.BOY, { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: "turn",
            frames: [{ key: CST.PLAYER.BOY, frame: 4 }],
            frameRate: 20
        });

        this.anims.create({
            key: "right",
            frames: this.anims.generateFrameNumbers(CST.PLAYER.BOY, { start: 5, end: 8 }),
            frameRate: 10,
            repeat: -1
        });


        // Botones de flechas
        flechaIzquierda = this.add.image(130, HeightSize - 55, CST.BUTTONS.LEFT).setInteractive();
        flechaIzquierda.on("pointerdown", () => { flecha = 1 }).on("pointerup", () => { flecha = 0 });

        flechaDerecha = this.add.image(320, HeightSize - 55, CST.BUTTONS.RIGHT).setInteractive();
        flechaDerecha.on("pointerdown", () => { flecha = 2 }).on("pointerup", () => { flecha = 0 });

        flechaArriba = this.add.image(widthSize - 130, HeightSize - 55, CST.BUTTONS.UP).setInteractive();
        flechaArriba.on("pointerdown", () => { flecha = 3 }).on("pointerup", () => { flecha = 0 });

        flechaAbajo = this.add.image(widthSize - 320, HeightSize - 55, CST.BUTTONS.DOWN).setInteractive();
        flechaAbajo.on("pointerdown", () => { flecha = 4 }).on("pointerup", () => { flecha = 0 });

        // Configuramos las estrellas que apareceran en el juego
        stars = this.physics.add.group({
            key: CST.IMAGE.STAR,
            repeat: Math.round(widthSize / 70 - 1),
            setXY: { x: 12, y: 0, stepX: 70 }
        });

        // Hacemos que las estrellas reboten cuando caen
        stars.children.iterate(function (child) {
            child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
        });

        //Creamos las bombas
        bombs = this.physics.add.group();

        // Creamos el jugador y añadimos fisica
        player = this.physics.add.sprite(30, HeightSize - 150, CST.PLAYER.BOY);
        player.setBounce(0.1);
        player.setCollideWorldBounds(true);

        // Colisiones
        this.physics.add.collider(player, platforms);
        this.physics.add.collider(stars, platforms);
        this.physics.add.collider(bombs, platforms);
        this.physics.add.collider(bombs, bombs);

        this.physics.add.collider(player, bombs, hitBomb, null, this);
        this.physics.add.overlap(player, stars, collectStar, null, this);

        //Reproducir sonido de fondo.
        this.sound.play(CST.AUDIO.MUSIC, { loop: true });

        // Strings
        scoreText = this.add.text(5, 5, "Score:0", { fontSize: "37px", fill: "#000" });
        vidasText = this.add.text(60, 50, vidas, { fontSize: "37px", fill: "#000" });

        btnPause = this.add.image(widthSize - 130, 30, CST.SVG.PAUSE).setInteractive();
        btnPause.on("pointerdown", () => {
            alert("PAUSE");
        });

        btnScreen = this.add.image(widthSize - 30, 30, CST.SPRITE.SCREENSIZE, 1).setInteractive();
        btnScreen.on("pointerdown", () => {
            if (!document.fullscreenElement) {
                btnScreen.setFrame(0);
                document.documentElement.requestFullscreen();
            } else {
                btnScreen.setFrame(1);
                document.exitFullscreen();
            }
        });

    }

    // Actualización de objetos
    update() {

        if (gameOver) {
            return;
        }

        if (score == masVelocidad) {
            masVelocidadX += 0.3;
            masVelocidad += 25;
        } else {
            backgrounSky.tilePositionX -= masVelocidadX;
        }

        if (cursors.left.isDown || flecha == 1) {
            player.setVelocityX(-300);
            player.anims.play("left", true);

        } else if (cursors.right.isDown || flecha == 2) {
            player.setVelocityX(300);
            player.anims.play("right", true);

        } else {
            player.setVelocityX(0);
            player.anims.play("turn");

        }


        if ((cursors.up.isDown || flecha == 3) && player.body.touching.down) {
            this.jump.play();
            player.setVelocityY(-400);

        } else if (cursors.down.isDown || flecha == 4) {
            player.setVelocityY(400);

        }

        if (score == darVida) {
            this.darVidaOgg.play();
            vidas += 1;
            vidasText.setText(vidas);
            darVida += 100;
        }

    }

}

function collectStar(player, star) {

    this.coin.play();
    star.disableBody(true, true);
    score += 1;
    scoreText.setText("Score:" + score);

    if (stars.countActive(true) === 0) {

        // A new batch of stars to collect
        stars.children.iterate(function (child) {

            child.enableBody(true, child.x, 0, true, true);

        });

        var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

        var bomb = bombs.create(x, 16, CST.IMAGE.BOMB); bomb.setBounce(1); bomb.setCollideWorldBounds(true);
        bomb.setVelocity(Phaser.Math.Between(-200, 200), 20); bomb.allowGravity = false;

    }

}

function hitBomb(player, bomb) {

    if (vidas == 1) {

        vidas -= 1;
        vidasText.setText(vidas);

        // Detenga la musica

        this.sound.play(CST.AUDIO.GAMEOVER, { loop: true });

        this.physics.pause(); player.setTint(0xff0000); player.anims.play("turn"); gameOver = true;

        setTimeout(segundos, 5000);

    } else {
        bomb.disableBody(true, true);
        this.bombOgg.play();
        vidas -= 1;
        vidasText.setText(vidas);
    }

}

function segundos() {
    if (confirm("Desea continuar jugando?")) { location.reload(); }
}